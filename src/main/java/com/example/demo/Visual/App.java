package com.example.demo.Visual;

import com.example.demo.Dominio.Empleado;
import com.example.demo.Dominio.EmpleadoRepository;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.criteria.CriteriaBuilder;

/**
 * Created by Edward on 18/07/2017.
 */
@SpringUI
public class App extends UI{
    @Autowired
    EmpleadoRepository empleadoRepository;



    protected void init(VaadinRequest vaadinRequest){
        VerticalLayout layout = new VerticalLayout();
        HorizontalLayout hlayout = new HorizontalLayout();
        Grid<Empleado> grid=new Grid<>();

        Label titulo=new Label("Registro de Empleados");
        TextField nombre=new TextField("Nombre");
        TextField edad=new TextField("Edad");
        TextField puesto=new TextField("Puesto");
        TextField salario=new TextField("Salario");

        Button add=new Button("Addicional");
        add.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Empleado e=new Empleado();

                e.setNombre(nombre.getValue());
                e.setEdad(edad.getValue());
                e.setPuesto(puesto.getValue());
                e.setSalario(salario.getValue());
                empleadoRepository.save(e);
                grid.setItems(empleadoRepository.findAll());
                nombre.clear();
                edad.clear();
                puesto.clear();
                salario.clear();
                Notification.show("Empleado adicionado");

            }
        });

        layout.addComponents(titulo);
        titulo.setStyleName("h2");
        layout.setComponentAlignment(titulo, Alignment.MIDDLE_CENTER);
        hlayout.addComponents(nombre, edad, puesto, salario, add);
        hlayout.setComponentAlignment(add, Alignment.BOTTOM_RIGHT);



        grid.addColumn(Empleado::getId).setCaption("Id");
        grid.addColumn(Empleado::getNombre).setCaption("Nombre");

        grid.addColumn(Empleado::getEdad).setCaption("Edad");
        grid.addColumn(Empleado::getPuesto).setCaption("Puesto");
        grid.addColumn(Empleado::getSalario).setCaption("Salario");

        layout.addComponents(hlayout);
        grid.setWidth("800px");
        layout.addComponents(grid);
        setContent(layout);

    }



}
